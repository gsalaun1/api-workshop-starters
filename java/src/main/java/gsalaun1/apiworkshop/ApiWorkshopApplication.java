package gsalaun1.apiworkshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiWorkshopApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiWorkshopApplication.class, args);
    }

}
