package gsalaun1.apiworkshop

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ApiWorkshopApplication

fun main(args: Array<String>) {
    runApplication<ApiWorkshopApplication>(*args)
}
