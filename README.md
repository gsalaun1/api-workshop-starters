# Exercice Spring

# Objectif

Valider les connaissances Spring

# Durée

Une heure 30 minutes maximum

# Métier

Construire une API pour manipuler une liste de langages de programmation.

Caractéristiques d'un langage de programmation :
- Id : number
- Typing (deux possibilités uniquement) : dynamic, static
- Name : string
- Year : number

Cette API doit répondre à l'ensemble des requêtes envoyées par le frontend exposé.